/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class Palindrome {
    
    public boolean isPalindrome(String a){
//        int i;
//        String b="";
        String s = a.toLowerCase();
//        System.out.println(s);
        String s1 = s.replaceAll("[,.?!:;-]", " ");
//        System.out.println(s1);
        String s2 = s1.replaceAll(" ","");
        

        //comparing the last and first element of given staring
        if(s2.length()==0||s2.length()==1){
            return true;
        }
        //using the recursive function of ispalindrome and substring of substracted index
        if(s2.charAt(0)==s2.charAt(s2.length()-1)){
            return isPalindrome(s2.substring(1, s2.length()-1));
        }
        return false;
    }
}

            
        
    
        

    

