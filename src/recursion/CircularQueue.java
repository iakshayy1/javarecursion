/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

/**
 *
 * @author Akshay Reddy Vontari
 * 
 */
public class CircularQueue extends Exception{
    private final int ARRAY_SIZE = 50;
    private int[] arr = new int[ARRAY_SIZE];
    private int front = -1;
    private int rear = -1;

    public CircularQueue() {
    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int getFront() {
        return front;
    }

    public void setFront(int front) {
        this.front = front;
    }

    public int getRear() {
        return rear;
    }

    public void setRear(int rear) {
        this.rear = rear;
    }
    //Insert method to insert element from rear.
    public void Insert(int a)
    {
        
        if((rear == ARRAY_SIZE-1 && front == 0)|| front==rear+1){
            System.out.println("Queue is full");
        }
        else{
            if(rear == ARRAY_SIZE-1 && front != 0){
                rear = -1;
            }
            //incrementing rear to assign value in queue
            rear++;
            arr[rear]=a;
            //assigning front to 0 if it is -1 to insert the value because we cannot insert value at -1
            if(front==-1){
                front = 0;
            }
            
        
        }
    }

    
    //remove method to remove element from the front.
    public void Remove(){
        if(front==-1&& rear == -1){
            System.out.println("\nQueue is empty");
        }
        //deleted element is from front so array of front is called
        System.out.println("Deleted element :"+arr[front++]);
        
      if(front == ARRAY_SIZE)
	 front = 0;
      if(front-1 == rear)
	 front = rear = -1;
//      arr[front++]=0;
   }


        
    //retrieve method to print the peek value
    public int Retrieve() throws emptyQueueException{
        if(front != -1 && rear != -1){
            //array front element is called beacuse peek value in a queue is front.
            return arr[front];
}
        else{
            throw new emptyQueueException("Empty queue");
        }
    }
    //length method to find queue length
    public int Length(){
        int a;
      if(front>rear){
          //considering maxsize of an array front index is added with rear and on whole substracted if front is greater than rear
          a=ARRAY_SIZE - front + rear + 1;
                }
      else{
          //if rear greater than front then we can find using index of front and rear; 
          a= rear - front + 1;
      }
    
      return a;
    }
    //print method using recursion
    public void printo() {
    if (front == rear) {
        return;
    }
    System.out.print(arr[front] + " ");
    front = (front + 1) % ARRAY_SIZE;
    printo();
}
    // print method without using recursion
    public void Print(){
        if(front==-1 && rear ==-1){
            System.out.println("Queue underflow");
        }
        else if(front>rear)
    {
        for(int i=0;i<=rear;i++)
            System.out.println(arr[i]);
        for(int j=front;j<=ARRAY_SIZE-1;j++)
            System.out.print(arr[j]+" ");
       
    }
    else
    {
//        System.out.println(front);
        for(int i=front;i<=rear;i++)
        {
            System.out.print(arr[i]+" ");
        }
        
    }
    }
    //isFull method to chech whether queue is full or not
    public boolean isFull(){
       return (rear == ARRAY_SIZE-1 && front == rear+1)|| (front==0);
    }
    //isEmpty method to chech whether queue is empty or not
    public boolean isEmpty(){
        return (front<=-1&& rear <= -1);
    }
    
    
    
}
