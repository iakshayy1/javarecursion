/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recursion;

import static java.lang.Math.random;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Akshay Reddy Vontari
 */
public class RecursionWorksheet {
    //recursive method to find gcd of two paraments
    public static int gcd(int a,int b){
        if(b>0){
            return gcd(b,a%b);
        }
        else
        {
            return a;
        }
        
    }
    //recursive method to find the sum of square of two numbers 
    public static int f(int n){
        if(n == 0){
            return 0;
        }
        else
        {
            return n*n+f(n-1);
        }
    }
// recursive method for factorial
    public static int fact(int n){
        if(n==0){
            return 1;
        }
        else
        {
            return n*fact(n-1);
        }
    }
    //recusive method for reverse of given number
    public static void mystery(long n)
	{
		System.out.print(n % 10);
		if ((n / 10) != 0)
		{
			mystery(n / 10);
		}
	}

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws emptyQueueException {
        // TODO code application logic here
        System.out.println(gcd(48,39));
        System.out.println(gcd(12,15));
        System.out.println(gcd(1234,456));
        System.out.println(f(3));
        System.out.println(f(5));
        System.out.println(f(10));
        System.out.println("This method returns the sum of squares of n numbers");
        System.out.println(fact(3));
        System.out.println(fact(5));
        System.out.println(fact(7));
        mystery(29);
        System.out.println("");
        mystery(12345);
        System.out.println("");
        System.out.println("This method prints the reverse of number what we entered as parameter.");
        Sorting s = new Sorting();
        int[] randomNum = new int[50];
        for(int i =0;i<randomNum.length;i++){
            randomNum[i] = (int)(Math.random()*50);
        }
        System.out.println(Arrays.toString(randomNum));
        System.out.println(Arrays.toString(s.Sort(randomNum,randomNum.length)));
        Scanner sc = new Scanner(System.in);
        Palindrome p = new Palindrome();
        String a = sc.nextLine();
        System.out.println("enter the string:"+a);
        if(p.isPalindrome(a)){
            System.out.println(a+" is a palindrome");
        }
        else{
            System.out.println(a+" is not a palindrome");
        }
        System.out.println("");
        CircularQueue c = new CircularQueue();
        Random r = new Random();
        //inserting 50 random numbers to demonstrate the methods
        for(int i=0;i<50;i++){
        c.Insert(r.nextInt(50));
        }
//        System.out.println(Arrays.toString(c.getArr()));
        c.Print();
        //determining whether the queue is full or not.
         if(c.isFull()==true){
            System.out.println("\nCircular Queue is Full.");
        }
        else{
            System.out.println("Circular queue is not full.");
        }
        //determining whether the queue is empty or not.
        if(c.isEmpty()==true){
            System.out.println("Circular queue is Empty.");
        }
        else
        {
            System.out.println("Circular queue is not Empty.");
        }
        System.out.println("Length the circular queue before delete :"+c.Length());
        //deleting two numbers to demonstrate that the numbers are deleting from front
        c.Remove();
        c.Remove();
        //printing the numbers after inserting and deleting of the numbers
//        System.out.println(Arrays.toString(c.getArr()));
        c.Print();
        //length of the circular queue
        System.out.println("\nLength the circular queue after delete :"+c.Length());
        //retriveing the front value from the queue
        System.out.println("Retrieved value at the front of the queue : "+c.Retrieve()); 
        //determining whether the queue is full or not.
        if(c.isFull()==true){
            System.out.println("\nCircular Queue is Full.");
        }
        else{
            System.out.println("Circular queue is not full.");
        }
        //determining whether the queue is empty or not.
        if(c.isEmpty()==true){
            System.out.println("Circular queue is Empty.");
        }
        else
        {
            System.out.println("Circular queue is not Empty.");
        }
        
        
        c.printo();
        
        
        
    }
    
}

        
